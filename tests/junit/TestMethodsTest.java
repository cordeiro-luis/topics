package junit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class TestMethodsTest {

    @Test
    @DisplayName("Add two numbers test - assertEquals")
    void addTest() {

        //delta 0, precise result only
        assertEquals(4, TestMethods.add(2, 2), 0);
    }

    @Test
    @DisplayName("Multiple operations test - assertAll")
    void multiplyTest() {
        assertAll(
                () -> assertEquals(4, TestMethods.multiply(2, 2)),
                () -> assertEquals(-4, TestMethods.add(-2, -2))
        );
    }


    @Test
    @DisplayName("Multiply two numbers using a Matcher test - assertThat ")
    void multiply2Test() {

        //evaluate using a third party matcher
        assertThat (  TestMethods.multiply(4, 4), is(equalTo(16.0)) );
    }


    @Test
    @DisplayName("Find min and max of integers array text - assertArrayEquals ")
    void minMaxTest() {

        int[] expectedResult = new int[] {1, 10};  //the min and max value

        assertArrayEquals(expectedResult, TestMethods.minMax(10, 2, 1, 3) );
    }


    @Test
    @DisplayName("Evaluate false condition - assertFalse ")
    void isValidFalseTest() {

        assertFalse( TestMethods.isValid("NOK") );
    }

    @Test
    @DisplayName("Evaluate true condition - assertTrue ")
    void isValidTrueTest() {

        assertTrue( TestMethods.isValid("OK") );
    }

    @Test
    @DisplayName("Evaluate object instance existance - assertNotNull ")
    void valueToObjectTest() {

        assertNotNull( TestMethods.valueToObjectString(Boolean.TRUE) );
    }

    @Test
    @DisplayName("Evaluate null object - assertNull ")
    void valueToObject2Test() {

        assertNull( TestMethods.valueToObjectString(null) );
    }

    @Test
    @DisplayName("Evaluate object instance is not the same - assertNotSame ")
    void valueToObject3Test() {

        //evaluate if the returned String object is the cached one or a new instance (thus not the same)
        assertNotSame(  Boolean.TRUE.toString(), TestMethods.valueToObjectString(Boolean.TRUE) );
    }

    @Test
    @DisplayName("Evaluate if given object instance is returned - assertSame ")
    void returnObjectInstanceTest() {

        String myString = "I'm an a String object";
        //evaluate if the returned String object is still the same instance
        assertSame(  myString, TestMethods.returnObjectInstance(myString) );
    }

    @Test
    @DisplayName("Evaluate if the returned object instance is of the expected type - assertInstance ")
    void returnObjectInstance2Test() {

        assertInstanceOf(  String.class, TestMethods.valueToObjectString(Boolean.TRUE) );
    }

    @Test
    @DisplayName("Evaluate exception - assertThrows ")
    void makeExceptionTest() {

        assertThrows( Exception.class, () -> {TestMethods.makeException();}  );
    }

    @Test
    @DisplayName("Evaluate timeout - assertTimeout ")
    void justSleepTest() {

        //simulate one second pause when the timeout is two seconds
        assertTimeout ( Duration.ofSeconds(2), () -> {TestMethods.justSleep(1);}  );
    }

}