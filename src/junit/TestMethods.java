package junit;

import java.util.Arrays;
import java.util.stream.DoubleStream;

public class TestMethods {

    //experiment with varargs and strems
    //https://docs.oracle.com/javase/1.5.0/docs/guide/language/varargs.html

    public static double add(double... operands) {
        return DoubleStream.of(operands)
                .sum();
    }

    public static double multiply(double... operands) {
        return DoubleStream.of(operands)
                .reduce(1, (a, b) -> a * b);
    }

    public static int[] minMax(int... intList) {

        int min = Arrays.stream(intList).min().getAsInt();
        int max = Arrays.stream(intList).max().getAsInt();

        return new int[] {min,max};
    }

    public static boolean isValid(String value) {

        return !value.equals("NOK");
    }

    public static String valueToObjectString(Boolean value) {

        if (value == null) return null;

        return new String( value.toString());  //creates a new String object
    }

    public static String returnObjectInstance(String value) {

        return value; //just return the same instance
    }


    public static void makeException() throws Exception {

        throw new Exception();
    }

    public static void justSleep(int i) throws InterruptedException {

        Thread.sleep(i*1000); //sleep "i" number of seconds before returning control
    }



}
