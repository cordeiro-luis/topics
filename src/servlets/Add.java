package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.LongStream;

@WebServlet(name = "Add", value = "/Add")
public class Add extends HttpServlet {
    private static long add(long... operands) {

        return LongStream.of(operands)
                .sum();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        out.println("<html>");
        out.println("<h1>GET not supported. Please go back and POST the operands</h1>");
        out.println("<a href=\"index.jsp\">Back</a>");
        out.println("</body></html>");

        out.close();

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        //For a simple html this is enough.
        out.println("<html> <style>\n" +
                ".alert {\n" +
                "  padding: 20px;\n" +
                "  background-color: #f44336;\n" +
                "  color: white;\n" +
                "}\n" +
                "\n" +
                ".closebtn {\n" +
                "  margin-left: 15px;\n" +
                "  color: white;\n" +
                "  font-weight: bold;\n" +
                "  float: right;\n" +
                "  font-size: 22px;\n" +
                "  line-height: 20px;\n" +
                "  cursor: pointer;\n" +
                "  transition: 0.3s;\n" +
                "}\n\n" +
                ".closebtn:hover {\n" +
                "  color: black;\n" +
                "}\n" +
                "</style>\n" +
                "<head><title>CIT 360 - L07 Servets</title></head><body>");

        //grab inputs
        try {
            long op1 = Long.parseLong(request.getParameter("op1"));
            long op2 = Long.parseLong(request.getParameter("op2"));

            out.println("<h1>");
            out.println(op1);
            out.println(" + ");
            out.println(op2);
            out.println(" = ");
            out.println(add(op1, op2));

            out.println("</h1>");

        } catch (java.lang.NumberFormatException ex) {

            out.println("<div class=\"alert\">\n" +
                    "  <span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span>\n" +
                    "  The given inputs are not valid integer numbers or out of range, impossible to proceed. Please correct\n" +
                    "</div>");
        }

        out.println("<br><br>");
        out.println("<a href=\"index.jsp\">Back</a>");
        out.println("</body></html>");

        out.close();

    }

}
