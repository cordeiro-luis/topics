package jasonhttpurl.server;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;


public class HttpListener {


    //my local server
    private static HttpServer server;


    public static void main(String[] args)  {

        //start the HTTP server on its own JVM and wait for requests
        //however, for this CIT360 topic, the server will start on the same JVM context
        //of the calling client.
        startHTTPServer();

    }

    //public method to allow starting the server from other classes
    public static int startHTTPServer() {

        try {

            server = HttpServer.create();
            server.bind(new InetSocketAddress("localhost", 8080), 2);

        } catch (IOException e) {
            System.out.println("\n[SERVER] Unable to start http listener: "+ e);
            return -1;
        }

        System.out.println("\n[SERVER] HTTP Listener Started. Accepting Requests: "+ server.getAddress() );

        //define a context for the incoming requests. Using a default path that fits all options
        HttpContext defaultContext = server.createContext("/");

        //set the handler that will process the incoming requests
        defaultContext.setHandler(new HttpJSONHandler() );

        //Starts this server in a new background thread (from API docs)
        server.start();

        //exit program in a clean way. Experiment for future weeks on CIT 360
        HttpServer finalServer = server;

        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {
                    System.out.println("\n[SERVER] Stopping HTTP listener");
                    finalServer.stop(2); //it will respond to pending requests and close the socket
                })
        );

        return 0;

    }

    //allows stopping the HTTP server from other classes
    public static void stopHTTPServer(){

        //stop the HTTP server
        server.stop(1);

    }

}
