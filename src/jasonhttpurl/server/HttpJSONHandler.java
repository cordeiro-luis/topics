package jasonhttpurl.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import jasonhttpurl.data.Family;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Random;

public class HttpJSONHandler implements HttpHandler {

    //Define a very simple handler. Only the HTTP GET is supported.
    //Also including support to reject other HTTP verbs besides GET, just for fun

    private static final String _GET = "GET";

    private static final int _ST_OK = 200;
    private static final int _ST_NOT_ALLOWED = 405;

    private static final String _H_ALLOW = "Allow";
    private static final String _H_CONTENT_TYPE = "Content-Type";

    private static final Charset _CHARSET = StandardCharsets.UTF_8;


    //built a database of family members to have dynamic JSON responses
    private static final String[] _FAMILY_NAMES = { "Luis", "Fatima", "Felicia", "Cássia", "Samuel", "Luis",
            "Maria", "Raquel", "Tiago","Rebeca"};
    private static final short[] _FAMILY_AGES = { 50, 49, 25, 22, 20, 17, 16, 14, 11,8};

    //handle method implementation
    public void handle(HttpExchange request) {

        System.out.println("[SERVER] Incoming Request from: "+ request.getRemoteAddress() + " at "+ Instant.now() );

        try (request) {

            Headers headers = request.getResponseHeaders();
            String requestMethod = request.getRequestMethod().toUpperCase();

            //the switch can be expanded to the other HTTP Verbs later, making this handler smarter
            //IntelliJ suggested a new enhanced switch, giving it a try
            switch (requestMethod) {
                case _GET -> {
                    String responseBody = getJSONstring(); //get a JSON string
                    headers.set(_H_CONTENT_TYPE, String.format("application/json; charset=%s", _CHARSET));

                    final byte[] rawResponseBody = responseBody.getBytes(_CHARSET);
                    request.sendResponseHeaders(_ST_OK, rawResponseBody.length);
                    request.getResponseBody().write(rawResponseBody);
                }
                default -> {
                    headers.set(_H_ALLOW, _GET);
                    request.sendResponseHeaders(_ST_NOT_ALLOWED, -1);
                }
            }
        } catch (IOException e) {
            System.out.println("[SERVER] Unable to process incoming HTTP request : " + e);
        }
    }

    //generate a JSON string out of a object instance
    private static String getJSONstring(){

        //random pick
        Random rand = new Random();
        int n = rand.nextInt(10);

        //create family member instance
        Family member = new Family();
        member.setName(_FAMILY_NAMES[n]);
        member.setAge(_FAMILY_AGES[n]);

        //let's make a JSON string from this object
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "[]"; //default to empty

        try {
            jsonString = mapper.writeValueAsString(member);
        } catch (JsonProcessingException e) {
            System.out.println("[SERVER] Unable to create JSON string : " + e);
        }

        System.out.println("[SERVER] Created JSON string: " + jsonString);

        return jsonString;
    }
}
