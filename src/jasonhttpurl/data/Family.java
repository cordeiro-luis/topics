package jasonhttpurl.data;

public class Family {

    private String name;
    private short age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getAge() {
        return age;
    }

    public void setAge(short age) {
        this.age = age;
    }

    public String toString() {
        return "Family Member ---> name: " + name + ", age: " + age;
    }

}
