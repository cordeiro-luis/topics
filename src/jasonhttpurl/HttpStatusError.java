package jasonhttpurl;

public class HttpStatusError extends Exception{

    private int status;

        public HttpStatusError(int status ) {
            this.status = status;
        }

        public String toString() {
            return "HTTP status not 200. Received status is " + status ;
        }


}
