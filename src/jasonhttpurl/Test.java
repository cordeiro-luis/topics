package jasonhttpurl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jasonhttpurl.data.Family;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Random;

import static jasonhttpurl.server.HttpListener.startHTTPServer;
import static jasonhttpurl.server.HttpListener.stopHTTPServer;

public class Test {

    public static void main(String[] args) {

        //one click test. Start the http server in the same JVM of the client.
        int status = startHTTPServer();
        if (status !=0) return;  //exit if the server does not start.


        //repeat test three times
        for (int i = 0; i <3; i++) {

            //get a remote JSON
            getRemoteJSON();

            //wait 2 seconds
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println("[CLIENT] Unable wait "+ e);
            }

        }

        //stop the HTTP server and complete the test
        stopHTTPServer();

    }

    private static void getRemoteJSON() {

        System.out.println("\n[CLIENT] HTTP GET for a JSON string");

        String response;
        try {
            response = getHttpContent("http://localhost:8080");
        } catch (IOException e) {
            System.out.println("[CLIENT] Unable to connect to HTTP server: "+ e);

            //don't do JSON parsing, no response exists
            return;

        } catch (InterruptedException e) {
            System.out.println("[CLIENT] Connection to HTTP server interrupted: "+ e);

            //don't do JSON parsing, no response exists
            return;

        } catch (HttpStatusError e) {
            System.out.println("[CLIENT]: "+ e);

            //don't do JSON parsing, no response exists
            return;
        }

        //parse the JSON string
        ObjectMapper mapper = new ObjectMapper();
        Family member = null;

        //create object instance out of the JSON string
        try {
            member = mapper.readValue(response, Family.class);
        } catch (JsonProcessingException e) {
            System.out.println("[CLIENT] Unable to parse the JSON string: "+ e);
        }

        //just printing the object. (not the JSON)
        System.out.println("[CLIENT] Printing the remotely received family member JSON into object --> "+ member);

    }


    // do the HTTP GET request using the specific and existing library to create an HTTP client
    //this library can be used to work with proxies, authentication, etc.
    private static String getHttpContent (String address) throws IOException, InterruptedException, HttpStatusError {

        //sample taken out and adapted from the API documentation
        HttpClient client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NEVER)
                .connectTimeout(Duration.ofSeconds(20))
                .build();


        //create a request builder
        HttpRequest.Builder builder = HttpRequest.newBuilder();
        builder.uri( URI.create(address) );

        //random pick of a GET or a DELETE HTTP verb, to test the server
        Random rand = new Random();
        int n = rand.nextInt(2);
        if (n!=0) builder.DELETE();

        HttpRequest request = builder.build();

        //Receive the HTTP Response
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        //evaluate if status 200 was received, if not throw exception
        if (response.statusCode() != 200) throw new HttpStatusError(response.statusCode());

        return response.body();

    }
}