package datastore;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class FamilyDB {

    private static final Map<String, FamilyMember> familyMembers = new HashMap();

    //populate my DB
    static {

        //set parents
        FamilyMember father = new FamilyMember("Luis", (short) 50, null, null);
        FamilyMember mother = new FamilyMember("Fátima", (short) 49, null, null);

        familyMembers.put(father.getName(), father);
        familyMembers.put(mother.getName(), mother);

        FamilyMember person = new FamilyMember("Felicia", (short) 26, father, mother);
        familyMembers.put(person.getName(), person);

        person = new FamilyMember("Cássia", (short) 23, father, mother);
        familyMembers.put(person.getName(), person);

        person = new FamilyMember("Samuel", (short) 20, father, mother);
        familyMembers.put(person.getName(), person);

        person = new FamilyMember("Luis", (short) 17, father, mother);
        familyMembers.put(person.getName(), person);

        person = new FamilyMember("Maria", (short) 16, father, mother);
        familyMembers.put(person.getName(), person);

        person = new FamilyMember("Raquel", (short) 14, father, mother);
        familyMembers.put(person.getName(), person);

        person = new FamilyMember("Tiago", (short) 11, father, mother);
        familyMembers.put(person.getName(), person);

        person = new FamilyMember("Rebeca", (short) 8, father, mother);
        familyMembers.put(person.getName(), person);

        //grandson
        person = new FamilyMember("Simão", (short) 5, null, familyMembers.get("Cássia"));
        familyMembers.put(person.getName(), person);

    }

    public static FamilyMember getPersonByName(String name) {
        return familyMembers.get(name);
    }

    public static FamilyMember getPersonFather(@NotNull FamilyMember person) {
        return familyMembers.get(person.getName()).getFather();
    }

    public static FamilyMember getPersonMother(FamilyMember person) {
        return familyMembers.get(person.getName()).getMother();
    }

    public static void setPersonAgeByName(String name, short age) throws InvalidAge, PersonNotFound {

        FamilyMember person = familyMembers.get(name);

        if (person == null) throw new PersonNotFound();

        //validate age.
        if (age < 0) throw new InvalidAge("Age cannot be negative");
        if (person.getFather().getAge() < age) throw new InvalidAge("Father cannot be younger");
        if (person.getMother().getAge() < age) throw new InvalidAge("Mother cannot be younger");

    }

}