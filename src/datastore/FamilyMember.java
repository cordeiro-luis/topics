package datastore;

public class FamilyMember {

    private String name;
    private short age;

    private FamilyMember father;
    private FamilyMember mother;

    //construtor
    protected FamilyMember(String name, short age, FamilyMember father, FamilyMember mother){
        this.name = name;
        this.age = age;
        this.father = father;
        this.mother = mother;
    }

    public String getName() {
        return name;
    }
    protected  void setName(String name) {
        this.name = name;
    }

    public short getAge() {
        return age;
    }

    protected  void setAge(short age) {
        this.age = age;
    }

    public FamilyMember getFather() {
        return father;
    }

    protected  void setFather(FamilyMember father) {
        this.father = father;
    }

    public FamilyMember getMother() {
        return mother;
    }

    protected  void setMother(FamilyMember mother) {
        this.mother = mother;
    }

    public String toString() {
        return "Family Member ---> name: " + name + ", age: " + age;
    }

}
