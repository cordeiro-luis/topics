package threads;

import java.time.Instant;
import java.util.Random;

public class Consumer implements Runnable {

    private final Repository repo;
    private final String name;

    public Consumer(Repository repo, String name) {
        this.repo = repo;
        this.name = name;
    }

    @Override
    public void run() {

        Random randNumber = new Random();
        String tId = String.valueOf(Thread.currentThread().getId());

        System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " - started thread (run method) that will consume work items");

        int i = 0;

        do {
            System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " is waiting for work items");

            try {
                synchronized (repo) {
                    repo.wait(randNumber.nextInt(9000) + 1000); //wait up to ten seconds for a notify.
                }
            } catch (InterruptedException ex) {
                System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " was interrupted.");
            }

            Integer workItem = repo.getFromWorkToBeDone();

            if (workItem == null) {
                i++; //count the number of times no work was found
            } else {
                System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " is processing work item # " + workItem);
                i = 0; //reset the counter. Consumer found work to do
            }

        } while (i <= 5); //only wait five times, at the end exit

        System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " found no work items to consume after five tries. Giving up and completing.");

    }
}
