package threads;

import java.util.PriorityQueue;

public class Repository {


    //The Repository class is a wrapper to enable the syncronization, the wait,
    // and notify work (instance level). The Collections have a way to syncronize, but this is a test
    private static final PriorityQueue workToBeDone = new PriorityQueue();

    public Repository() {
    }

    //These are the methods used by the threads. Access to these methods
    // needs to be synchronized to avoid collisions
    public synchronized Integer getFromWorkToBeDone() {
        return (Integer) workToBeDone.poll();
    }

    public synchronized void putFromWorkToBeDone(Integer todo) {
        workToBeDone.offer(todo);
        return;
    }

}
