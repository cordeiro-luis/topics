package threads;

import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class Test {

    //create a pool of three threads to perform the two tests
    private static final ExecutorService executor = Executors.newFixedThreadPool(3);

    public static void main(String[] args) {

        testExecutorService();

        System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n");

        testProducerConsumer();

        //shutdown all threads after completion and exit program
        executor.shutdown();
    }

    //////////////////////////
    // Test the ExecutorService and atomic variables
    //////////////////////////

    public static void testExecutorService() {

        System.out.println("[MAIN] " + Instant.now() + " ------start test for ExecutorService --------");

        //Use the executor to experiment an atomic variable behavior under stress.
        //An even and equal number of increment a decrement working threads
        //guarantees that the final "counter" is always zero, which was the initial value.

        System.out.println("[MAIN] " + Instant.now() + " - initial atomic counter value: " + MyAtomicVariable.currentCounterValue());
        System.out.println("[MAIN] " + Instant.now() + " - instantiate 4 objects of class MyAtomicVariable that implement Runnable");

        MyAtomicVariable atAdd1 = new MyAtomicVariable(MyAtomicVariable.ADD);
        MyAtomicVariable atSub1 = new MyAtomicVariable(MyAtomicVariable.SUBTRACT);

        MyAtomicVariable atAdd2 = new MyAtomicVariable(MyAtomicVariable.ADD);
        MyAtomicVariable atSub2 = new MyAtomicVariable(MyAtomicVariable.SUBTRACT);

        System.out.println("[MAIN] " + Instant.now() + " - starting 4 working threads that increment and decrement atomic value");

        //let's start the threads. Just for fun, let's start less threads than
        //the ones we needed. Let's imagine my CPU only has three cores (never seen one)
        //The ExecutorServide is a class object
        Future tAdd1 = executor.submit(atAdd1);
        Future tSub1 = executor.submit(atSub1);
        Future tAdd2 = executor.submit(atAdd2);
        Future tSub2 = executor.submit(atSub2);


        //are all the thread completed? If yes, let's just verify the final atomic
        //variable value
        while (!(tAdd1.isDone()
                && tSub1.isDone()
                && tAdd2.isDone()
                && tSub2.isDone())) {

            System.out.println("[MAIN] " + Instant.now() + " - working threads not done yet - Wait 100 millisecond");

            //wait one second to give time
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                //do nothing
            }
        }


        System.out.println("[MAIN] " + Instant.now() + " - all the threads completed, verify the final atomic variable value:" +
                MyAtomicVariable.currentCounterValue());

        System.out.println("[MAIN] " + Instant.now() + " ------end test for ExecutorService --------");

    }


    /////////////////////////////////////
    // Test a producer consumer sample
    /////////////////////////////////////

    public static void testProducerConsumer() {

        System.out.println("[MAIN] " + Instant.now() + " ------start test for producer consumer threads --------");

        //new "work" repository that the threads use. It's an instance object
        //so the synchronization can be tested
        Repository repo = new Repository();

        // we need to set up a producer of objects (it could be more)
        executor.execute(new Producer(repo, 5, "Producer_A"));

        // and we need at least two consumers, to make it more interesting
        executor.execute(new Consumer(repo, "Consumer_A"));
        executor.execute(new Consumer(repo, "Consumer_B"));

    }

}
