package threads;

import java.time.Instant;
import java.util.Random;


public class Producer implements Runnable {

    private final Repository repo;
    private final String name;
    private int maxProducerCycles = 0;

    public Producer(Repository repo, int maxProducerCycles, String name) {
        this.maxProducerCycles = maxProducerCycles;
        this.repo = repo;
        this.name = name;
    }

    @Override
    public void run() {

        Random randNumber = new Random();
        String tId = String.valueOf(Thread.currentThread().getId());

        System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " started thread (run method). It will produce " + maxProducerCycles + " work items");

        for (int i = 1; i <= maxProducerCycles; i++) {

            Integer rndN = randNumber.nextInt(20);
            System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " is making work item #" + i + " with content: " + rndN);

            // put the work item (the method is synchronized
            Integer work = Integer.valueOf(rndN);
            repo.putFromWorkToBeDone(work);

            synchronized (repo) {
                //warn the consumers that a work item is ready to be picked.
                repo.notifyAll();
            }

            System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " is resting");

            try {
                //sleep from 1 second up to 3 seconds
                Thread.sleep(randNumber.nextInt(2000) + 1000);
            } catch (InterruptedException ex) {
                System.out.println("\t[" + tId + "] " + Instant.now() + " - " + name + " was interrupted. No more work items.");
            }
        }

        System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + name + " completed his task. Done!");
        //at the end of these producing cycle the thread will complete and exit
    }

}
