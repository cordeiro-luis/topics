package threads;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;

public class MyAtomicVariable implements Runnable {

    public static final String ADD = "increments";
    public static final String SUBTRACT = "decrements";

    //note that i is a class variable and not an instance variable.
    //no explicit synchronized blocks are used here. The atomic variable takes
    //care of concurrent access and changes
    private static final int COUNTER_VALUE = 100; //initial counter value

    private static final AtomicInteger i = new AtomicInteger(COUNTER_VALUE);
    private final String clientType;

    //constructor
    public MyAtomicVariable(String clientType) {
        this.clientType = clientType;
    }

    public static int currentCounterValue() {
        return i.get();
    }

    private int putOne() {
        return i.incrementAndGet();
    }

    private int takeOne() {
        return i.decrementAndGet();
    }

    // Run in the context of an asynchronous thread
    // Each thread is set to either increment or decrement the atomic variable
    @Override
    public void run() {

        String tId = String.valueOf(Thread.currentThread().getId());

        //loop 10 times very fast to "stress test" the atomic variable
        int c = 0;
        System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - started thread (run method) that does loop of 10 fast calculations");

        for (int i = 0; i <= 10; i++) {

            if (clientType.equals(ADD)) c = putOne();  //increment atomic variable
            if (clientType.equals(SUBTRACT)) c = takeOne(); //decrement atomic variable

            System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - " + i + " - " + clientType + " atomic variable. The current value is: " + c);

        }

        System.out.println("\t[tid=" + tId + "] " + Instant.now() + " - completed. The atomic variable value was:" + c);

    }
}