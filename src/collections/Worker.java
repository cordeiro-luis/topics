package collections;

import java.util.Comparator;

public class Worker {

    private final String lastName;
    private final String firstName;
    private final int yearsInService;
    private final int insertionOrder;

    public Worker(String firstName, String lastName, int yearsInService, int insertionOrder) throws Exception
    {

        if (lastName==null || firstName==null || yearsInService < 0  || insertionOrder < 1)
            throw new Exception("Not possible to instantiate new Worker. Field validations error.");

        this.lastName = lastName;
        this.firstName = firstName;
        this.yearsInService = yearsInService;
        this.insertionOrder = insertionOrder;
    }

    // Used to print student details in main()
    public String toString()
    {
        return "(original insert order: "+ this.insertionOrder + ") " + this.firstName + " " + this.lastName + " is working for " + this.yearsInService + " years.";
    }

    // simplified class, no getters are provided, only a toString is exposed publicly

     static class SortByFirstName implements Comparator<Worker> {
        // Sorting in ascending order of first name
        public int compare(Worker w1, Worker w2)
        {
            return   w1.firstName.compareTo(w2.firstName);
        }
    }

     static class SortByYears implements Comparator<Worker> {
        // Sorting in ascending order for years in service

        // the API explains that the compare method, "Returns a negative integer, zero, or a positive integer as
        // the first argument is less than, equal to, or greater than the second.". So, a subtraction can be used to
        // achieve this.

        public int compare(Worker w1, Worker w2)
        {
            return   w1.yearsInService - w2.yearsInService;
        }
    }

}
