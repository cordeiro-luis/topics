/*
 * This is the Sample for Collections
 */
package collections;

import java.util.*;

/**
 *
 * @author cordeirol
 */
@SuppressWarnings("ALL")
public class Samples {

    /**
     * Test collections
     * @author cordeirol
     */
    public static void testCollections() {

        System.out.println("------start test collections--------");

        testHashMap();
        testTreeMap();
        testArrayList();
        testTreeSet();
        testLinkedList();
        testQueue();
        testSortingArrayList();

        System.out.println("------end test collections--------");

    }

    /**
     * Test HashMap for dictionary
     * @author cordeirol
     */
    private static void testHashMap() {

        System.out.println("------test HashMap--------");
        System.out.println("--What if we want a dictionary for a quick lookup of words. This is accomplished with a HasMap.");

        // create a simple dictionary
        Map<String, String> words = new HashMap();

        words.put("Hash", " to talk about (something)");
        words.put("Academic", "relating to schools and education");
        //... and so on

        //get a word meaning
        String meaning = words.get("Academic");

        System.out.println("The meaning of Academic is: " + meaning);

        System.out.print("\n");

    }



    /**
     * Test TreeMap for sorted dictionary
     * @author cordeirol
     */
    private static void testTreeMap() {

        System.out.println("------test TreeMap--------");
        System.out.println("--What if we want to sort in a special way? This is automatic with a TreeMap");

        // create a simple dictionary
        Map<Integer, String> positions = new TreeMap<>();

        //add entries randomly
        positions.put(Integer.valueOf(9), "IX");
        positions.put(Integer.valueOf(1), "I");
        positions.put(Integer.valueOf(5), "V");
        positions.put(Integer.valueOf(3), "III");
        //... and so on


        System.out.println("--Print the entries sorted using the key set...");

        //print out the position meanings  in order. An extract the key set is used to iterate. this Set is ordered.
        Set keys = positions.keySet();
        for (Object value : keys) {
            System.out.println( positions.get(value) );
        }

        System.out.print("\n");

        System.out.println("--The same can be done with the entry set... it will include the keys");

        //print out the position meanings  in order. An extract the key set is used to iterate. this Set is ordered.
        Set entries = positions.entrySet();
        for (Object value : entries) {
            System.out.println( value );
        }

        System.out.print("\n");

    }


    /**
     * Test ArrayList
     * @author cordeirol
     */
    private static void testArrayList() {

        System.out.println("------test ArrayList--------");

        // create a simple word list
        List<String> words = new ArrayList<>();  //no predefined size is necessary
        words.add("Hash");
        words.add("Academic");
        //... and so on

        //get the second word
        String word = words.get(1);

        System.out.println("The second word is: " + word);

        //get the full list of words
        System.out.println("The word list is: ");
        for (String value : words) {
            System.out.println("word: " + value);
        }

        //or we can loop the List and fetch the values on the Map
        System.out.print("\n");
    }


    /**
     * Test TreeMap for sorted dictionary
     * @author cordeirol
     */
    private static void testTreeSet() {

        System.out.println("------test TreeSet--------");
        System.out.println("--What if we want a Set that is sorted? This is automatic with a TreeSet");

        // create a simple dictionary
        Set<Integer> positions = new TreeSet<>();

        //add entries randomly
        positions.add(10);  //the add method accepts a primitive
        positions.add(Integer.valueOf(1));
        positions.add(Integer.valueOf(5));

        positions.add( Integer.valueOf(3) );


        //let's try to add another instance of an object that represents the same comparable int. This is not allowed
        //on this collection, and it will not show up on the test results, confirming it.
        //this constructor was deprecated in java 9, however the intent of the experiment is to have different instance
        // representing the -->same comparable value<---. If valueOf methods was used, it would just
        // return a cached version of the same instance

        positions.add( new Integer(3) );
        positions.add( new Integer(3) );
        positions.add( new Integer(3) );

        //... and so on

        //print out the position in order.
        for (Integer value : positions) {
            System.out.println( value.toString() );
        }

        System.out.println("--The Set does not allow multiple objects (instances) that are comparable " +
                "Although the 3 was added multiple time, it only shows once");

        System.out.print("\n");

    }


    /**
     * Test TreeMap for sorted dictionary
     * @author cordeirol
     */
    private static void testLinkedList() {

        System.out.println("------test LinkedList--------");
        System.out.println("--What if we want a Set that is not sorted and works in a FIFO manner? This is automatic with a LinkedList");

        // create a simple list
        List<Integer> positions = new LinkedList<>();

        //add entries randomly
        positions.add(Integer.valueOf(10));
        positions.add(Integer.valueOf(1));
        positions.add(Integer.valueOf(5));

        positions.add( Integer.valueOf(3) );

        //let try to add other instances of an object that represents the same int. This is allowed in LinkedList
        //this method "valueOf" returned a cached instance representing that int value. So, the same instance multiple
        //times.

        positions.add( Integer.valueOf(3) );
        positions.add( Integer.valueOf(3)  );
        positions.add( Integer.valueOf(3)  );

        //... and so on

        //print out the position in order.
        for (Integer value : positions) {
            System.out.println( value.toString() );
        }

        System.out.println("--The List does allows multiple objects (instances) that are comparable, to be a part of the set. Observe the FIFO behavior.");

        System.out.print("\n");

    }

    /**
     * Test Queue
     * @author cordeirol
     */
    private static void testQueue() {

        System.out.println("------test Queue--------");
        System.out.println("--What if we want a Queue to add and poll?");

        // create a simple queue
        Queue<String> positions = new PriorityQueue<String>();

        //add "persons" on the queue
        positions.add("Person 1");
        positions.add("Person 2");
        positions.add("Person 3");


        //Because it's a priority queue, the order maitained. Let's remove the head of the queue and keep it

        String person=null;

        person = positions.poll();
        System.out.print("who is in the queue first? ");
        System.out.println(person);

        person = positions.poll();
        System.out.print("who is in the queue second? ");
        System.out.println(person);

        person = positions.poll();
        System.out.print("who is in the queue third? ");
        System.out.println(person);

        System.out.print("How many persons are in the queue now? ");
        System.out.println(positions.size());

        System.out.print("\n");
    }

    /**
     * Test ArrayList
     * @author cordeirol
     */
    private static void testSortingArrayList() {

        System.out.println("------test sorting an ArrayList using the Comparable interface--------");

        // create a simple company workers list
        List<Worker> companyWorkers = new ArrayList<Worker>();  //no predefined size is necessary

        try {

            companyWorkers.add(new Worker("Rebeca", "Cordeiro", 5, 1));
            companyWorkers.add(new Worker("Luis", "Cordeiro", 25, 2));
            companyWorkers.add(new Worker("Felicia", "Cordeiro", 20, 3));
            companyWorkers.add(new Worker("Fátima", "Cordeiro", 24, 4));

        } catch (Exception e) {
            System.out.println(e.toString());
            return;
        }

        //... and so on

        System.out.println("--print by insertion order");

        for (Worker employee : companyWorkers) {
            System.out.println("--> " + employee);
        }

        System.out.print("\n");

        System.out.println("--print by first name order");

        Collections.sort(companyWorkers, new Worker.SortByFirstName() );

        for (Worker employee : companyWorkers) {
            System.out.println("--> " + employee);
        }

        System.out.print("\n");

        //////////////

        System.out.println("--print by year of service order");

        Collections.sort(companyWorkers, new Worker.SortByYears() );

        for (Worker employee : companyWorkers) {
            System.out.println("--> " + employee);
        }
    }

}
