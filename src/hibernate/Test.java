package hibernate;


import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import hibernate.data.Family;
import hibernate.util.HibernateUtil;



public class Test {

    public static void main(String[] args) {

        //add two sample rows
        Family dad = new Family("Luis", Date.valueOf( LocalDate.of(1971,6,19) ),"luis@cordeiro.sytes.net");
        Family mom = new Family("Fátima", Date.valueOf( LocalDate.of(1972,3,11) ),"fatima@cordeiro.sytes.net");

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student objects
            session.save(dad);
            session.save(mom);
            // commit transaction
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        //list all rows from the DB. Extract the generated identity field for later use
        List<Long> allIDs = new ArrayList();

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            System.out.println("------- Retrieve a result set of all rows --------");
            List <Family> members = session.createQuery("from hibernate.data.Family", Family.class).list();
            for (Family person : members) {
                Long id = person.getId();
                System.out.println(id + " - " + person.getFirstName() + " was born on " + person.getDateOfBirth().toString() );
                allIDs.add(id);
            }

            //retrieve data row by row
            System.out.println("\n\n------- Retrieve each row, id by id --------");
            for( Long id : allIDs) {
                Family person = session.createQuery("from hibernate.data.Family where id="+id, Family.class).getSingleResult();
                System.out.println(person.getId() + " - " + person.getFirstName() + " email is " + person.getEmail() );
            }

            session.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        HibernateUtil.shutdown();

    }
}
