package errorHandlingDataValidation;

import java.math.BigDecimal;
import java.util.Scanner;

public class UserInputs {

    private final static Scanner keyboard = new Scanner(System.in);

    public static BigDecimal getNumerator() throws NumberFormatException{
        return new BigDecimal(keyboard.nextLine());
    }

    public static BigDecimal getDenominator() throws NumberFormatException, ZeroValue{

        BigDecimal inputValue=new BigDecimal(keyboard.nextLine());

        if (inputValue.equals(BigDecimal.ZERO)) {
            throw new ZeroValue();
        }

        return inputValue;
    }


}
