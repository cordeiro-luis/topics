package errorHandlingDataValidation;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Test {

    public static void main(String[] args) {

        BigDecimal numerator=null;
        BigDecimal denominator=null;
        boolean invalid = true;

        //get numerator
        do {

            System.out.println("Please provide the numerator:");

            try {
                numerator = UserInputs.getNumerator();
                invalid = false; //i.e. the numerator is ok
            } catch (NumberFormatException ex) {
                System.out.println("The numerator value is invalid. Try again.");
            }

        } while (invalid);


        invalid = true;
        //get denominator
        do {

            System.out.println("Please provide the denominator:");

            try {
                denominator = UserInputs.getDenominator();
                invalid = false; //i.e. the denominator is ok
            } catch (NumberFormatException ex) {
                System.out.println("The denominator value is invalid. Try again.");
            }
            catch (ZeroValue ex) {
                System.out.println("The denominator cannot be zero. Try again.");
            }

        } while (invalid);

        System.out.println("---------------------");

        try {
            System.out.print("the quotient of the division is: " + numerator.divide(denominator));
        }catch (ArithmeticException ex) {
            System.out.println("The quotient cannot be represented exactly. Applying a round up and fixed decimal size");
            System.out.print("A rounded quotient of the division is: " + numerator.divide(denominator,10, RoundingMode.HALF_UP));
        }

    }

}