package errorHandlingDataValidation;

public class ZeroValue extends Exception{

    public ZeroValue() {
    }

    public String toString() {
        return "zero value not allowed";
    }

}
